import random
import uuid
import zmq
from response_status_code import ResponseStatusCode
from crypt import client_hmac_validator, server_hmac_validator


servers_addresses = []
host = "tcp://*:8888"

context = zmq.Context()

socket = context.socket(zmq.REP)
socket.bind(host)


def receiver():
    while True:
        req = socket.recv_json()
        print(req)
        if client_hmac_validator(req["header"]["hmac"], req["data"]):
            json_data = req["data"]
            rep = client_handler(json_data)
            reply(rep)
        elif server_hmac_validator(req["header"]["hmac"], req["data"]):
            json_data = req["data"]
            rep = server_handler(json_data)
            reply(rep)
        else:
            pass


def basic_reply_form(status_code, request, content):
    return {"status": status_code, "response_to": request, "content": content}


def reply(json_obj):
    socket.send_json(json_obj)


def load_balance():
    return random.choice(servers_addresses)


def client_handler(req):
    if req["request"] == "connect":
        server = load_balance()
        content = [server, str(uuid.uuid4())]
        res = basic_reply_form(ResponseStatusCode.OK, req["request"], content)
        return res
    return basic_reply_form(ResponseStatusCode.NOT_FOUND, req["request"], None)


def server_handler(req):
    if req["request"] == "add_to_server_list":
        ip = req["args"][0]
        rec_port = req["args"][1]
        send_port = req["args"][2]
        address = (ip, rec_port, send_port)
        if address in servers_addresses:
            return basic_reply_form(ResponseStatusCode.ALREADY_DONE, req["request"], None)
        else:
            servers_addresses.append(address)
            return basic_reply_form(ResponseStatusCode.CREATED, req["request"], None)
    return basic_reply_form(ResponseStatusCode.NOT_FOUND, req["request"], None)


receiver()
