import json
import queue
import zmq
import threading
from response_status_code import ResponseStatusCode
from config import SERVER_HMAC_KEY
import crypt
import time

ip = "127.0.0.1"
rec_port = "8870"
send_port = "8871"


msg_queue = queue.Queue()


class ConnectToDispatcher(threading.Thread):
    def __init__(self, address, context):
        context = context if context else zmq.Context()
        threading.Thread.__init__(self)
        self.socket = context.socket(zmq.REQ)
        self.socket.connect(address)
        self.socket.RCVTIMEO = 2000  # millisecond
        self.last_message = None
        self.command = None
        self.trigger = False

    def send_json(self, json_obj):
        self.last_message = json_obj
        self.socket.send_json(json_obj)

    def receive_message(self):
        try:
            print(self.socket.recv_json())
        except zmq.error.Again:
            print("timeout...")

    def init_in_dispatcher(self):
        message = basic_request_form("add_to_server_list", [ip, rec_port, send_port])
        self.command = basic_data_form(message)
        self.trigger = True

    def run(self):
        self.init_in_dispatcher()
        while True:
            if self.trigger:
                self.send_json(self.command)
                self.receive_message()
            self.trigger = False
            time.sleep(1)


class Receiver(threading.Thread):
    def __init__(self, context):
        threading.Thread.__init__(self)
        context = context if context else zmq.Context()
        self.socket = context.socket(zmq.ROUTER)
        self.socket.bind("tcp://*:" + rec_port)

    def _receive(self):
        while True:
            address, msg = self.socket.recv_multipart()
            print(address, msg)
            handler(address, msg)

    def run(self):
        self._receive()


class Sender(threading.Thread):
    def __init__(self, context):
        threading.Thread.__init__(self)
        context = context if context else zmq.Context()
        self.socket = context.socket(zmq.ROUTER)
        self.socket.bind("tcp://*:" + send_port)

    def _send(self):
        while True:
            lst = msg_queue.get()
            address = lst[0]
            msg_json = lst[1]
            byte_msg = json.dumps(msg_json).encode()
            print(byte_msg)
            self.socket.send_multipart([address, byte_msg])

    def run(self):
        self._send()


def basic_data_form(data, header={}):
    str_data = json.dumps(data)
    hmac_data = crypt.hmac_creator(SERVER_HMAC_KEY, str_data)
    new_header = header
    new_header["hmac"] = hmac_data
    return {"header": new_header,
            "data": data}


def basic_reply_form(status_code, request, content):
    return {"status": status_code, "response-to": request, "content": content}


def basic_request_form(req, args=None):
    return {"request": req, "args": args}


def handler(address, msg):
    msg_str = msg.decode()
    msg_json = json.loads(msg_str)
    if msg_json["data"]["request"] == "echo":
        res = basic_reply_form(ResponseStatusCode.OK, msg_json["data"]["request"], msg_json["data"])
        msg_queue.put([address, res])
    elif msg_json["data"]["request"] == "match_request":
        pass

CONTEXT = zmq.Context()

ctd = ConnectToDispatcher("tcp://localhost:8888", CONTEXT)
ctd.start()

r = Receiver(CONTEXT)
s = Sender(CONTEXT)

r.start()
s.start()




