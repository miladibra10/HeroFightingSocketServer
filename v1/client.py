import threading
import zmq
import queue
import uuid

send_queue = queue.Queue()

context = zmq.Context()
worker_identity = b"False"
IDENTITY = uuid.uuid4().bytes

# Socket For Sending message to broker
sender_socket = context.socket(zmq.DEALER)
sender_socket.setsockopt(zmq.IDENTITY, IDENTITY)
sender_socket.connect('tcp://localhost:5566')

# Socket For Receiving message from broker
receiver_socket = context.socket(zmq.DEALER)
receiver_socket.setsockopt(zmq.IDENTITY, IDENTITY)
receiver_socket.connect("tcp://localhost:5567")


def send_message(message):
    sender_socket.send_multipart([worker_identity, message])


def receive_message():
    while True:
        result = receiver_socket.recv_json()
        print(result)


def first_request():
    send_message(b'first_req')


first_request()

threading.Thread(target=receive_message, args=()).start()
