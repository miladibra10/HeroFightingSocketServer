import threading
import zmq
import random
import json

# for saving worker addresses
workers = []


# Function Threads


def client_handshake():
    pass


def clients_to_workers():
    while True:
        client_address, worker_identity, req = from_front.recv_multipart()
        print("client:", end="  ")
        print(client_address, worker_identity, req)
        if worker_identity != b"False":
            to_back.send_multipart([worker_identity, client_address, req])
        else:
            worker_address = random.choice(workers)
            req_to_worker = {"data": "tell_your_id_to_client"}
            req_to_worker_json = json.dumps(req_to_worker)
            req_to_worker_json_bytes = bytes(req_to_worker_json.encode())
            to_back.send_multipart([worker_address, client_address, req_to_worker_json_bytes])


def workers_to_clients():
    while True:
        w_address, c_address, resp = from_back.recv_multipart()
        print("worker: ", end=" ")
        print(w_address, c_address, resp)
        to_front.send_multipart([c_address, resp])


def workers_request_handler():
    while True:
        address, req = back_handler.recv_multipart()
        print("worker req: ", end="  ")
        print(req)
        dict_req = json.loads(req)
        if dict_req["request"] == "set_my_identity":
            workers.append(bytes(dict_req["identity"].encode()))
            back_handler.send_json({"status": 'ok'})


context = zmq.Context()

# Socket for Receiving from Client
from_front = context.socket(zmq.ROUTER)
from_front.bind("tcp://*:5566")

# Socket for Sending to Client
to_front = context.socket(zmq.ROUTER)
to_front.bind("tcp://*:5567")

# Socket for Receiving from Worker
from_back = context.socket(zmq.ROUTER)
from_back.bind("tcp://*:5577")

# Socket for Sending to Client
to_back = context.socket(zmq.ROUTER)
to_back.bind("tcp://*:5578")

# Socket for handling backend Requests
back_handler = context.socket(zmq.ROUTER)
back_handler.bind("tcp://*:5579")

threading.Thread(target=clients_to_workers, args=()).start()
threading.Thread(target=workers_to_clients, args=()).start()
threading.Thread(target=workers_request_handler, args=()).start()
