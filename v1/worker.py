import json
import threading
import zmq
import queue


worker_identity = b"w2"
request_queue = queue.Queue()


context = zmq.Context()

# Socket for Sending back Response to Broker
sender_socket = context.socket(zmq.DEALER)
sender_socket.connect('tcp://localhost:5577')


#Socket for Recieving client message from Broker
receiver_socket = context.socket(zmq.DEALER)
receiver_socket.setsockopt(zmq.IDENTITY, worker_identity)
receiver_socket.connect("tcp://localhost:5578")


#Socket for Sending Handling Requests to Broker
to_broker = context.socket(zmq.DEALER)
to_broker.connect("tcp://localhost:5579")


def response_base_format(json_obj):
    return bytes(json.dumps(json_obj).encode())


def receiver():
    while True:
        user_address, request = receiver_socket.recv_multipart()
        print(user_address, request)
        decoded_request = request.decode()
        request_json = json.loads(decoded_request)
        request_queue.put((user_address, request_json))


def set_and_send_identity_to_broker():
    to_broker.send_json({"request": "set_my_identity", "identity": worker_identity.decode()})


def handler():
    address, request = request_queue.get()
    while request:
        if request["data"] == "tell_your_id_to_client":
            result = {"data": "server_identity", "identity": worker_identity.decode()}
            result_in_base = response_base_format(result)
            sender_socket.send_multipart([address, result_in_base])

        address, request = request_queue.get()


#Setting identity of worker in Broker
set_and_send_identity_to_broker()

threading.Thread(target=receiver, args=()).start()
threading.Thread(target=handler, args=()).start()
