

class ResponseStatusCode:
    OK = 200
    CREATED = 201
    ACCEPTED = 202
    ALREADY_DONE = 203
    BAD_REQUEST = 400
    FORBIDDEN = 403
    NOT_FOUND = 404

    def __init__(self):
        pass