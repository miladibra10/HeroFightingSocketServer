import hashlib
import base64
import hmac
import random
import json
from config import GENERAL_HMAC_KEY, SERVER_HMAC_KEY


def validate_hmac(key, old_hmac, data):
    """
    :return True if hmac is valid
    """
    data = json.dumps(data)
    if not check_byte_instance(data):
        if check_string_instance(data):
            data = data.encode()
    dig = hmac.new(key, data, hashlib.sha256).hexdigest().encode()
    b64_dig = base64.b64encode(dig).decode()
    if b64_dig == old_hmac:
        return True
    return False


def client_hmac_validator(old_hmac, data):
    return validate_hmac(GENERAL_HMAC_KEY, old_hmac, data)


def server_hmac_validator(old_hmac, data):
    return validate_hmac(SERVER_HMAC_KEY, old_hmac, data)


def hmac_creator(key, data):
    if not check_byte_instance(data):
        if check_string_instance(data):
            data = data.encode()
    dig = hmac.new(key, data, hashlib.sha256).hexdigest().encode()
    b64_dig = base64.b64encode(dig).decode()
    return b64_dig


def check_byte_instance(x):
    return isinstance(x, bytes)


def check_string_instance(x):
    return isinstance(x, str)
