import threading

import time
import zmq
import hashlib
import base64
import hmac
import json


address = "tcp://localhost:8888"
KEY = b"NimbleKnight"
context = zmq.Context()


def hand_shaker():
    socket = context.socket(zmq.REQ)
    socket.connect(address)
    req = {"data": {"request": "connect"}, "header": {}}
    data_byte = json.dumps(req["data"]).encode()
    data_hamc = hmac.new(KEY, data_byte, hashlib.sha256).hexdigest().encode()
    b64 = base64.b64encode(data_hamc).decode()
    req["header"]["hmac"] = b64
    socket.send_json(req)
    rep = socket.recv_json()
    socket.disconnect(address)
    socket.close()
    return rep["content"]


[ip, recv_port, send_port], IDENTITY = hand_shaker()
print(ip, recv_port, send_port, IDENTITY)
IDENTITY = IDENTITY.encode()


sender_socket = context.socket(zmq.DEALER)
sender_socket.setsockopt(zmq.IDENTITY, IDENTITY)
sender_socket.connect("tcp://" + ip + ":" + recv_port)


receiver_socket = context.socket(zmq.DEALER)
receiver_socket.setsockopt(zmq.IDENTITY, IDENTITY)
receiver_socket.connect("tcp://" + ip + ":" + send_port)


def send(data1):
    req = {"data": data1, "header": {}}
    data_byte = json.dumps(req["data"]).encode()
    data_hamc = hmac.new(KEY, data_byte, hashlib.sha256).hexdigest().encode()
    b64 = base64.b64encode(data_hamc).decode()
    req["header"]["hmac"] = b64
    sender_socket.send_json(req)
    print("sent..!")


def req_basic_form(user_id, request, args):
    return {"user_id": user_id, "request": request, "args": args}


def receiver():
    while True:
        print("receiving...")
        res = receiver_socket.recv()
        print(res)

time.sleep(1)
threading.Thread(target=receiver, args=()).start()


# sending echo:
data = req_basic_form("echo", None)

send(data)

